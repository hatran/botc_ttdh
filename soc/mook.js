var mook = {
    sobn: function () {
        let raw = `
        Vụ Ngân sách Nhà nước
        Vụ Đầu tư
        Vụ Tài chính hành chính sự nghiệp
        Vụ Chính sách thuế
        Vụ Tài chính các ngân hàng và tổ chức tài chính
        Cục quản lý, giám sát kế toán, kiểm toán
        Vụ Hợp tác quốc tế`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    tinhuy: function () {
        let raw = `Tổng Cục Thuế
        Tổng Cục Hải Quan
        Kho Bạc Nhà Nước
        Tổng Cục Dự Trữ Nhà Nước
        Ủy Ban Chứng Khoán Nhà Nước

        Cục Kế Toán Tài Chính`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    ubnd: function () {
        let raw = `Sở tài chính Hà Nội
                Sở tài chính TP Hồ Chí Minh
                Sở tài chính Quảng Ninh
                Sở tài chính Cần Thơ
                Sở tài chính Kiên Giang
                Sở tài chính Thái Nguyên
                Sở tài chính Nam ĐỊnh
                Sở tài chính An Giang
                Sở tài chính Thừa Thiên Huế
                Sở tài chính Bạc Liêu
                Sở tài chính Đồng Nai
                Sở tài chính Đồng Tháp
                Sở tài chính Sóc Trắng
                Sở tài chính Bà Rịa - Vũng Tàu
                Sở tài chính Bình Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },
    cdv: function () {
        let raw = `Sở tài chính Hà Nội
                Sở tài chính TP Hồ Chí Minh
                Sở tài chính Quảng Ninh
                Sở tài chính Cần Thơ
                Sở tài chính Kiên Giang
                Sở tài chính Thái Nguyên
                Sở tài chính Nam ĐỊnh
                Sở tài chính An Giang
                Sở tài chính Thừa Thiên Huế
                Sở tài chính Bạc Liêu
                Sở tài chính Đồng Nai
                Sở tài chính Đồng Tháp
                Sở tài chính Sóc Trắng
                Sở tài chính Bà Rịa - Vũng Tàu
                Sở tài chính Bình Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    },

    http: function () {
        let raw = `
        Sở tài chính Hà Nội
                Sở tài chính TP Hồ Chí Minh
                Sở tài chính Quảng Ninh
                Sở tài chính Cần Thơ
                Sở tài chính Kiên Giang
                Sở tài chính Thái Nguyên
                Sở tài chính Nam ĐỊnh
                Sở tài chính An Giang
                Sở tài chính Thừa Thiên Huế
                Sở tài chính Bạc Liêu
                Sở tài chính Đồng Nai
                Sở tài chính Đồng Tháp
                Sở tài chính Sóc Trắng
                Sở tài chính Bà Rịa - Vũng Tàu
                Sở tài chính Bình Dương`;
        let items = raw.split(/\n/);
        items = items.map(function (e) {
            return e.trim();
        });
        return items;
    }
}

